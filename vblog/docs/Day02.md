## 用户管理模块接口定义
### 上午内容 
+ User结构体定义及结构体嵌套
```go
// 用户创建成功后返回一个User对象
// CreatedAt 为啥没用time.Time, int64(TimeStamp), 统一标准化, 避免时区你的程序产生影响
// 在需要对时间进行展示的时候，由前端根据具体展示当前时区的时间
type User struct {
	// 系统参数
	*common.Meta
	// 用户参数
	*CreateUserRequest
}
// 系统通用参数
type Meta struct {
    Id        int   `json:"id" gorm:""`
    CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
    UpdatedAt int64 `json:"updated_at" gorm:"column:updated_at"`
}

// User对象用户参数
type CreateUserRequest struct {
    Username string            `json:"username" validate:"required" gorm:"column:username"`
    Password string            `json:"password" validate:"required" gorm:"column:password"`
    Role     Role              `json:"role" gorm:"column:role"`
    Label    map[string]string `json:"label" gorm:"column:label;serializer:json"`
}
```
+ 系统参数Meta结构体抽离到common包中 并构建对应的构造函数
```go
package common

import "time"

func NewMeta() *Meta {
	return &Meta{
		CreatedAt: time.Now().Unix(),
	}
}

// 系统通用参数
type Meta struct {
	Id        int   `json:"id" gorm:""`
	CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	UpdatedAt int64 `json:"updated_at" gorm:"column:updated_at"`
}
```
+ 用户角色的枚举值定义
```go
type Role int

const (
	Role_VISTOR Role = iota
	Role_Admin
)
```
+ User对象的构造函数定义
```go
func NewUser(req *CreateUserRequest) *User {
	return &User{
		Meta:              common.NewMeta(),
		CreateUserRequest: req,
	}
}
```
+ 接口的定义
```go
// 用户管理接口定义
type Service interface {
    // 用户创建
    // 1. 用户取消了请求怎么办?
    // 2. 后面要做Trace, Trace ID怎么传递进来
    // 3. 多个接口 需要做事务(Session), 这个事务对象怎么传递进来
    CreateUser(context.Context, *CreateUserRequest) (*User, error)
    // 用户查询
    // 总共有多个用户
    QueryUser(context.Context, *QueryUserRequest) (*UserSet, error)

```
+ 用户列表返回的结构体定义及构造函数
```go
func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

type UserSet struct {
	Total int     `json:"total"`
	Items []*User `json:"items"`
}
```
+ 分页请求的结构体定义及构造函数
```go
package common

func NewPageRequest() *PageRequest {
	return &PageRequest{
		PageSize:   10,
		PageNumber: 1,
	}
}

type PageRequest struct {
	PageSize   int
	PageNumber int
}

```
+ 查询用户请求的结构体定义及构造函数
```go
func NewQueryUserRequest() *QueryUserRequest {
    return &QueryUserRequest{
    PageRequest: common.NewPageRequest(),
    }
}

type QueryUserRequest struct {
    *common.PageRequest
}

func (i *UserServiceImpl) QueryUser(ctx context.Context, in *user.QueryUserRequest) (*user.UserSet, error) {
	return nil, nil
}
```
+ 创建用户的方法实现及单元测试
```go
func (i *UserServiceImpl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (*user.User, error) {
	// 1、校验请求的合法性
	if err := common.Validate(in); err != nil {
		return nil, err
	}

	// 2、创建user对象（资源）
	ins := user.NewUser(in)

	// 3、user对象保存入库
	/**
		- 读取数据库配置
		- 获取数据库连接
		- 操作数据库连接  保存数据
	**/
	if err := i.db.Save(ins).Error; err != nil {
		return nil, err
	}
	// 4、返回保存后的user实例对象

	return ins, nil
}
// 单元测试
var (
    serviceImpl user.Service
    ctx         = context.Background()
)

func init() {
    serviceImpl = impl.NewUserServiceImpl()
}

func TestCreateUser(t *testing.T) {
    req := user.NewCreateUserRequest()
    req.Username = "admin"
    req.Password = "123456"
    ins, err := serviceImpl.CreateUser(ctx, req)
    if err != nil {
        t.Fatal(err)
    }
    t.Log(ins)
}
```


### 下午内容

+ 定义配置对象
```go
type Config struct {
    Application *application `toml:"app" yaml:"app" json:"app"`
    MySQl       *mySQL       `toml:"mysql" yaml:"mysql" json:"mysql"`
}

func (c *Config) ToYAML() string {
    out, _ := yaml.Marshal(c)
    return string(out)
}

// 应用服务
type application struct {
    Host string `tomal:"host" yaml:"host" json:"host"`
    Port int    `tomal:"port" yaml:"port" json:"port"`
}

// mysql db对象
type mySQL struct {
    Host     string `tomal:"host" yaml:"host" json:"host" env:"DATASOURCE_HOST"`
    Port     int    `tomal:"port" yaml:"port" json:"port" env:"DATASOURCE_PORT"`
    DB       string `tomal:"database" yaml:"database" json:"database" env:"DATASOURCE_DB"`
    Username string `tomal:"username" yaml:"username" json:"username" env:"DATASOURCE_USERNAME"`
    Password string `tomal:"password" yaml:"password" json:"password" env:"DATASOURCE_PASSWORD"`
    Debug    bool   `tomal:"debug" yaml:"debug" json:"debug" env:"DATASOURCE_DEBUG"`
    db       *gorm.DB
    lock     sync.Mutex
}
```
+ 配置初始化
```go
func Default() *Config {
	return &Config{
		Application: &application{
			Host: "127.0.0.1",
			Port: 8080,
		},
		MySQl: &mySQL{
			Host:     "127.0.0.1",
			Port:     3306,
			DB:       "vblog",
			Username: "root",
			Password: "123456",
			Debug:    true,
		},
	}
}

```
+ 配置加载LoadConfig
```go
// 配置加载
var config *Config

func C() *Config {
	if config == nil {
		config = Default()
	}
	return config
}

```
+ 从环境变量加载配置文件
```go
import (
    "github.com/caarlos0/env/v6"
    "gopkg.in/yaml.v3"
    "os"
)

func LoadConfigFromEnv() error {
	config = C()
	return env.Parse(config)
}

```
+ 从yaml文件加载配置文件
```go
import (
    "github.com/caarlos0/env/v6"
    "gopkg.in/yaml.v3"
    "os"
)

func LoadConfigFromYaml(configPath string) error {
	content, err := os.ReadFile(configPath)
	if err != nil {
		return err
	}
	config = C()

	return yaml.Unmarshal(content, config)
}

```
+ Mysql配置提供单例对象
```go
// dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
func (m *mySQL) DSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		m.Username,
		m.Password,
		m.Host,
		m.Port,
		m.DB,
	)
}

func (m *mySQL) GetDB() *gorm.DB {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.db == nil {
		db, err := gorm.Open(mysql.Open(m.DSN()), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		m.db = db

		// 补充Debug配置
		if m.Debug {
			m.db = db.Debug()
		}
	}

	return m.db
}
```
+ 加载数据库对象依赖
```go
func NewUserServiceImpl() *UserServiceImpl {
	// 启动程序需要加载配置
	return &UserServiceImpl{
		db: conf.C().MySQl.GetDB(),
	}
}

type UserServiceImpl struct {
	db *gorm.DB
}
```

+ 创建用户的实现
```go
func (i *UserServiceImpl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (*user.User, error) {
	// 1、校验请求的合法性
	if err := common.Validate(in); err != nil {
		return nil, err
	}

	// 2、创建user对象（资源）
	ins := user.NewUser(in)

	// 3、user对象保存入库
	/**
		- 读取数据库配置
		- 获取数据库连接
		- 操作数据库连接  保存数据
	**/
	if err := i.db.Save(ins).Error; err != nil {
		return nil, err
	}
	// 4、返回保存后的user实例对象
	return ins, nil
}
```

+ 创建用户 单元测试
```go
var (
      serviceImpl user.Service
      ctx         = context.Background()
)

func init() {
    serviceImpl = impl.NewUserServiceImpl()
}
func TestCreateUser(t *testing.T) {
    req := user.NewCreateUserRequest()
    req.Username = "admin"
    req.Password = "123456"
    ins, err := serviceImpl.CreateUser(ctx, req)
    if err != nil {
        t.Fatal(err)
    }
    t.Log(ins)
}

=== RUN   TestCreateUser

2024/09/11 16:23:43 C:/Users/wangmw/GolandProjects/vblog/vblog/apps/user/impl/impl.go:37
[10.532ms] [rows:1] INSERT INTO `users` (`created_at`,`updated_at`,`username`,`password`,`role`,`label`) VALUES (1726043023,1726043023,'admin','123456',0,'{}')
impl_test.go:26: {
    "id": 3,
    "created_at": 1726043023,
    "updated_at": 1726043023,
    "username": "admin",
    "password": "123456",
    "role": 0,
    "label": {}
}
--- PASS: TestCreateUser (0.02s)
PASS

```
