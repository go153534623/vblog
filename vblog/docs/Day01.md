+ Restful 风格API设计
  + Restful: (Resource) Representational State Transfer(资源状态转换) API 风格
    + Resource Representational: 资源定义(服务端对象或者数据库内的一行纪录)
    + State Transfer:   创建/修改/删除
    + 资源定义 
      - 一类资源
        - /vblogs/api/v1/blogs: blogs 就是资源的类型: blogs 博客
        - /vblogs/api/v1/users: users 就是资源的类型: users 用户
      - 一个资源
        - /vblogs/api/v1/users/1: 1 就是资源的id, id为1的资源
    + 状态转换: 通过HTTP Method来定义资源的状态转化, 理解为用户针对某类或者某个资源的动作
      - POST: 创建一个类型的资源, POST /vblogs/api/v1/users 创建一个用户, 具体的参数存放在body
      - PATCH: 部分修改(补丁), PATCH /vblogs/api/v1/users/1, 对id为1的用户 做属性的部分修改, name:abc ("usera" ---> "abc")
      - PUT: 全量修改(覆盖), PUT /vblogs/api/v1/users/1, 对id为1的用户 做属性的全量修改, name:abc  除去name之外的所有属性全部清空
      - DELETE: 资源删除
      - GET: 获取一类资源: GET /vblogs/api/v1/users, 获取一个资源 GET /vblogs/api/v1/users/1

+ 认证方案
    + Basic Auth
    + Base Token
        + 令牌颁发方案
            + 客户端输入凭据向服务端发起认证请求（申请访问令牌）
            + 服务端验证客户端凭据 如果正确 则生成一个JWT的访问令牌（token）并返回给客户端 客户端会把token进行存储
            + 客户端在后续每次请求 都会把token作为认证头的一部分 发送给服务器进行验证
            + 服务器通过验证token的有效性确定token的合法性
            ![令牌颁发方案](../image/令牌颁发方案.png)

        + 令牌刷新方案
            + 客户端输入凭据（如用户名和密码）发起认证请求（申请访问令牌和刷新令牌）
            + 服务器验证客户端凭据后 生成访问令牌和刷新令牌 并返回给客户端
            + 客户端在请求中携带访问令牌 访问受保护的资源
            + 访问令牌过期后 客户端向服务端发送刷新令牌 请求新的访问令牌
            + 服务器验证刷新令牌的有效性和合法性 如果正确 返回新的访问令牌和刷新令牌
            + 客户端本地存储新的访问令牌
          
        ![令牌刷新方案](../image/令牌刷新方案.png)

+ 博客系统整体功能设计

![概要设计](../image/概要设计.png)


+ 关于单元测试
    + 被测试文件名：文件名_test.go
    + 被测试包名：包名_test
    + 被测试函数
      + 名称： Test被测试函数名
      + 签名：(t *test.Testing)
+ debug单步调试
  + 在你需要排除的代码行左侧打上断点
  + Debug运行调试代码
    + Continue: 到下一个断点
    + Step Over: 下一步
    + Step In: 单步调试
    + Step Out: 跳出单步调试
+ 功能分区架构(MVC)

![MVC](../image/功能分区.png)
+ 业务分区架构(模块化)

![DDD](../image/DDD.png)
