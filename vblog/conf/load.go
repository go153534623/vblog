package conf

import (
	"github.com/caarlos0/env/v6"
	"gopkg.in/yaml.v3"
	"os"
)

// 配置加载
var config *Config

func C() *Config {
	if config == nil {
		config = Default()
	}
	return config
}
func LoadConfigFromYaml(configPath string) error {
	content, err := os.ReadFile(configPath)
	if err != nil {
		return err
	}
	config = C()

	return yaml.Unmarshal(content, config)
}
func LoadConfigFromEnv() error {
	config = C()
	return env.Parse(config)
}
