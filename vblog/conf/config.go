package conf

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"sync"
)

func Default() *Config {
	return &Config{
		Application: &application{
			Host: "127.0.0.1",
			Port: 8080,
		},
		MySQl: &mySQL{
			Host:     "127.0.0.1",
			Port:     3306,
			DB:       "vblog",
			Username: "root",
			Password: "123456",
			Debug:    true,
		},
	}
}

type Config struct {
	Application *application `toml:"app" yaml:"app" json:"app"`
	MySQl       *mySQL       `toml:"mysql" yaml:"mysql" json:"mysql"`
}

func (c *Config) ToYAML() string {
	out, _ := yaml.Marshal(c)
	return string(out)
}

// 应用服务
type application struct {
	Host string `tomal:"host" yaml:"host" json:"host"`
	Port int    `tomal:"port" yaml:"port" json:"port"`
}

// mysql db对象
type mySQL struct {
	Host     string `tomal:"host" yaml:"host" json:"host" env:"DATASOURCE_HOST"`
	Port     int    `tomal:"port" yaml:"port" json:"port" env:"DATASOURCE_PORT"`
	DB       string `tomal:"database" yaml:"database" json:"database" env:"DATASOURCE_DB"`
	Username string `tomal:"username" yaml:"username" json:"username" env:"DATASOURCE_USERNAME"`
	Password string `tomal:"password" yaml:"password" json:"password" env:"DATASOURCE_PASSWORD"`
	Debug    bool   `tomal:"debug" yaml:"debug" json:"debug" env:"DATASOURCE_DEBUG"`
	db       *gorm.DB
	lock     sync.Mutex
}

func (m *mySQL) DSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		m.Username,
		m.Password,
		m.Host,
		m.Port,
		m.DB,
	)
}
func (m *mySQL) GetDB() *gorm.DB {
	m.lock.Lock()
	defer m.lock.Unlock()
	if m.db == nil {
		db, err := gorm.Open(mysql.Open(m.DSN()), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		m.db = db

		if m.Debug {
			m.db = db.Debug()
		}

	}

	return m.db

}
