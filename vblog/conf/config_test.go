package conf_test

import (
	"gitlab.com/go153534623/vblog/vblog/conf"
	"os"
	"testing"
)

func TestConfig_ToYAML(t *testing.T) {
	t.Log(conf.Default().ToYAML())
}
func TestLoadConfigFromYaml(t *testing.T) {
	err := conf.LoadConfigFromYaml("./application.yaml")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C().ToYAML())
}
func TestLoadConfigFromEnv(t *testing.T) {
	os.Setenv("DATASOURCE_DB", "vblog_wmw")
	err := conf.LoadConfigFromEnv()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C().ToYAML())
}
