package impl_test

import (
	"context"
	"gitlab.com/go153534623/vblog/vblog/apps/user"
	"gitlab.com/go153534623/vblog/vblog/apps/user/impl"
	"testing"
)

var (
	serviceImpl user.Service
	ctx         = context.Background()
)

func init() {
	serviceImpl = impl.NewUserServiceImpl()
}
func TestCreateUser(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.Username = "admin"
	req.Password = "123456"
	ins, err := serviceImpl.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryUser(t *testing.T) {
	req := user.NewQueryUserRequest()
	ins, err := serviceImpl.QueryUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)

}
