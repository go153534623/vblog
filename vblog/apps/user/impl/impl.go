package impl

import (
	"context"
	"gitlab.com/go153534623/vblog/vblog/apps/user"
	"gitlab.com/go153534623/vblog/vblog/common"
	"gitlab.com/go153534623/vblog/vblog/conf"
	"gorm.io/gorm"
)

func NewUserServiceImpl() *UserServiceImpl {
	// 启动程序需要加载配置
	return &UserServiceImpl{
		db: conf.C().MySQl.GetDB(),
	}
}

type UserServiceImpl struct {
	db *gorm.DB
}

func (i *UserServiceImpl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (*user.User, error) {
	// 1、校验请求的合法性
	if err := common.Validate(in); err != nil {
		return nil, err
	}

	// 2、创建user对象（资源）
	ins := user.NewUser(in)

	// 3、user对象保存入库
	/**
		- 读取数据库配置
		- 获取数据库连接
		- 操作数据库连接  保存数据
	**/
	if err := i.db.Save(ins).Error; err != nil {
		return nil, err
	}
	// 4、返回保存后的user实例对象

	return ins, nil
}

func (i *UserServiceImpl) QueryUser(ctx context.Context, in *user.QueryUserRequest) (*user.UserSet, error) {
	return nil, nil
}
