package user

import (
	"context"
	"gitlab.com/go153534623/vblog/vblog/common"
)

// 用户管理接口
type Service interface {
	//用户创建
	CreateUser(context.Context, *CreateUserRequest) (*User, error)
	//用户查询
	QueryUser(context.Context, *QueryUserRequest) (*UserSet, error)
}

func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		PageRequest: common.NewPageRequest(),
	}
}

type QueryUserRequest struct {
	*common.PageRequest
}
