package user

import (
	"encoding/json"
	"fmt"
	"gitlab.com/go153534623/vblog/vblog/common"
)

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Role:  Role_VISTOR,
		Label: map[string]string{},
	}
}

// User对象用户参数
type CreateUserRequest struct {
	Username string            `json:"username" validate:"required" gorm:"column:username"`
	Password string            `json:"password" validate:"required" gorm:"column:password"`
	Role     Role              `json:"role" gorm:"column:role"`
	Label    map[string]string `json:"label" gorm:"column:label;serializer:json"`
}

// 初始化校验器 validator.New()
func (req *CreateUserRequest) Validate() error {

	if req.Username == "" {
		return fmt.Errorf("用户名必填")
	}
	return nil
}

func NewUser(req *CreateUserRequest) *User {
	return &User{
		Meta:              common.NewMeta(),
		CreateUserRequest: req,
	}
}

// 结构体嵌套
type User struct {
	*common.Meta
	*CreateUserRequest
}

func (req *User) String() string {
	dj, _ := json.MarshalIndent(req, "", " 	")
	return string(dj)
}

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

type UserSet struct {
	Total int     `json:"total"`
	Items []*User `json:"items"`
}
