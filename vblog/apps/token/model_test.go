package token_test

import (
	"gitlab.com/go153534623/vblog/vblog/apps/token"
	test "testing"
)

func TestToken_String(a *test.T) {
	tk := token.Token{
		UserId:   "admin",
		Username: "管理员",
		Role:     "admin",
	}
	a.Log(tk.String())
}
