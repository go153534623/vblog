package token

import "encoding/json"

type Token struct {
	UserId                string `json:"user_id" gorm:"column:user_id"`
	Username              string `json:"username" gorm:"column:username"`
	AccessToken           string `json:"access_token" gorm:"column:access_token"`
	RefreshToken          string `json:"refresh_token" gorm:"column:refresh_token"`
	AccessTokenExpiredAt  int    `json:"access_token_expired_at" gorm:"column:access_token_expired_at"`
	RefreshTokenExpiredAt int    `json:"refresh_token_expired_at" gorm:"column:refresh_token_expired_at"`
	CreatedAt             int64  `json:"created_at" gorm:"column:created_at"`
	UpdatedAt             int64  `json:"updated_at" gorm:"column:updated_at"`
	Role                  string `json:"role" gorm:"-"`
}

func (t *Token) String() string {
	dj, _ := json.MarshalIndent(t, "=====", "    ")
	return string(dj)
}
