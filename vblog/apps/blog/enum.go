package blog

type Status int

const (
	Status_DRAFT = iota
	Publish
)
