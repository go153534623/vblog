package blog

import (
	"encoding/json"
	"gitlab.com/go153534623/vblog/vblog/common"
	"time"
)

// NewBlog 构造函数  构造Blog
func NewBlog() *Blog {
	return &Blog{
		Meta: &common.Meta{
			CreatedAt: time.Now().Unix(),
		},
		CreateBlogRequest: &CreateBlogRequest{
			Tags: map[string]string{},
		},
		ChangeBlogStatusRequest: &ChangeBlogStatusRequest{},
	}
}

type Blog struct {
	*common.Meta
	*CreateBlogRequest
	*ChangeBlogStatusRequest
}

func (req *Blog) String() string {
	dj, _ := json.MarshalIndent(req, "", " 	")
	return string(dj)
}

// 用户参数 用户传入
type CreateBlogRequest struct {
	Title    string            `json:"title" gorm:"column:title" validate:"required"`
	Author   string            `json:"author" gorm:"column:author" validate:"required"`
	Content  string            `json:"content" gorm:"column:content" validate:"required"`
	Summary  string            `json:"summary" gorm:"column:summary"`
	CreateBy string            `json:"create_by" gorm:"column:create_by"`
	Tags     map[string]string `json:"tags" gorm:"column:tags;serializer:json"`
}

func (req *CreateBlogRequest) String() string {
	dj, _ := json.MarshalIndent(req, "", " 	")
	return string(dj)
}

// 发布状态
type ChangeBlogStatusRequest struct {
	PublishedAt int64   `json:"published_at" gorm:"column:published_at"`
	Status      *Status `json:"status" gorm:"column:status"`
}

func (req *ChangeBlogStatusRequest) String() string {
	dj, _ := json.MarshalIndent(req, "", " 	")
	return string(dj)
}
