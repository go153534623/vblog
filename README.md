### vblog Markdown 博客系统
+ 项目技术栈与架构
    ![项目技术栈架构](vblog/image/项目技术架构1.png)
+ 项目结构
  +  main.go: 入口文件
  +  conf: 程序的配置处理
  +  exception: 业务自定义异常
  +  response: 请求返会的统一数据格式: {"code": 0, "msg": ""}
  +  protocol: 协议服务器
  +  apps: 业务模块开发区域
  +  image: 图片资源
  +  docs: 存放设计流程图文件和每日学习内容markdown文件